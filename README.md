# Test Automation Tutorials

This is mainly for guiding students from Test Automation class at Department of Mathematics and Computer Science, Faculty of Mathematics, HCMUSTest.
The main practices are same for all courses, but the IDE-in-use and the programming language are different. Additional, there are multiple assignments included in the reposity but your submissions are based on the requests on the go.

### Unit test automation tutorials and assignments
* Branch `eclipse-java`
  > * Used for practicing with Eclipse IDE and Java JUnit framework
  > * Check out the code as `git clone --single-branch --branch eclipse-java https://gitlab.com/endity/test-automation-tutorials.git`

* Branch `vs-csharp`
  > - Used for practicing with Microsoft Visual Studio and C# NUnit framework
  > - Check out the code as `git clone --single-branch --branch vs-csharp https://gitlab.com/endity/test-automation-tutorials.git`

